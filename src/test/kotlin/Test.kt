import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import kotlin.test.Test
import kotlin.test.assertSame
import kotlin.test.assertTrue
import kotlin.test.fail

class Test {

    @Test fun `verify two empty list returns null`() {
        assertTrue(null == getIntersection(null, null))
    }

    @Test fun `verify one empty list returns null`() {
        assertTrue(null == getIntersection(null, Node('a')))
        assertTrue(null == getIntersection(Node('a'), null))
    }

    @Test fun `verify two non-null lists with no intersection returns null`() {
        val list1 = createLinkedList('a', 'b', 'c')
        val list2 = createLinkedList('x', 'y', 'z')
        assertTrue(null == getIntersection(list1, list2))

        val list3 = createLinkedList('a', 'b', 'c')
        assertTrue(null == getIntersection(list1, list3))
    }

    @Test fun `verify two non-null lists with an intersection returns the correct node`() {
        val list1 = createLinkedList('a', 'b', 'c')
        val list2 = createLinkedList('x', 'y', 'z')
        val list3 = createLinkedList('d')
        list3?.joinToList(list1 ?: fail("Invalid list1!"))
        list3?.joinToList(list2 ?: fail("Invalid list2!"))
        assertSame(list3, getIntersection(list1, list2))
    }

    @Test fun `verify two non-null lists of different lengths with an intersection returns the correct node`() {
        val list1 = createLinkedList('a', 'b', 'c')
        val list2 = createLinkedList('x', 'y')
        list1?.joinToList(list2 ?: fail("Invalid lists!"))
        assertSame(list1, getIntersection(list1, list2))
    }

    @Test fun `verify three non-null lists of different lengths with an intersection returns the correct node`() {
        val list1 = createLinkedList('a')
        val list2 = createLinkedList('x', 'y', 'z')
        val list3 = createLinkedList('b','c')
        list3?.joinToList(list1 ?: fail("Invalid lists!"))
        list3?.joinToList(list2 ?: fail("Invalid lists!"))
        assertSame(list3, getIntersection(list1, list2))
    }

}

fun <T> Node<T>.joinToList(list: Node<T>) {
    tailrec fun getTail(current: Node<T>): Node<T> {
        val next = current.next ?: return current
        return getTail(next)
    }

    val tail = getTail(list)
    tail.next = this
}