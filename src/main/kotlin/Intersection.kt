import com.pajato.ctci.Node

fun getIntersection(list1: Node<Char>?, list2: Node<Char>?): Node<Char>? {
    tailrec fun getIntersection(
        set: MutableSet<Node<Char>>,
        current1: Node<Char>?,
        current2: Node<Char>?): Node<Char>? {
        fun addToSet() {
            if (current1 != null) { set.add(current1) }
            if (current2 != null) { set.add(current2) }
        }

        if (current1 == null && current2 == null) return null
        if (current1 === current2 || set.contains(current1)) return current1
        if (set.contains(current2)) return current2
        addToSet()
        return getIntersection(set, current1?.next, current2?.next)
    }

    return if (list1 == null || list2 == null) null else getIntersection(mutableSetOf(), list1, list2)
}
